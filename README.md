## Splaton Calculator
Splatoon Calculator is a program based on angularJS that allows you to calculate which abilities you can get on your gear.

## Usage
To use, [click here](https://calc.splatoon.ink)
